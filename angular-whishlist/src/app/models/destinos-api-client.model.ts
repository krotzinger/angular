import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './destino-viaje.model';

export class DestinosApiClient {
	destinos: DestinoViaje[];
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
	constructor() {
		this.destinos = [];
	}
	add(d: DestinoViaje) {
		this.destinos.push(d);
	}
	getAll() {
		return this.destinos;
	}
	select(d: DestinoViaje) {
		this.destinos.forEach(function (x) { x.unselect(); });
		d.select();
		this.current.next(d);
	}
	subscribeOnChange(fn) {
		this.current.subscribe(fn);
	}
}