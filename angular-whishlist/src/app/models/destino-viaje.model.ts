export class DestinoViaje {
	selected: boolean;
	public servicios: string[];

	constructor(public nombre:string, public descripcion:string, public imagenUrl:string) {
		this.servicios = ["pileta", "desayuno"]
		this.selected = false;
	}

	isSelected():boolean {
		return this.selected;
	}

	select() {
		this.selected = true;
	}

	unselect() {
		this.selected = false;
	}
}