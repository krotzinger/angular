import { TitleCasePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParam(this.minLongitud)])],
      descripcion: [''],
      url: ['', Validators.required]
    });

    this.fg.valueChanges.subscribe((form: any) => {
        console.log("cambio el formulario: ", form);
      });
   }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 3),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('/assets/datos.json'))
    ).subscribe(ajaxResponse => {
      console.log(ajaxResponse.response);
      this.searchResults = (ajaxResponse.response as string []).filter(s => s.includes(elemNombre.value));
    });
    fromEvent(elemNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length <= 3)
    ).subscribe(() => {
      this.searchResults = [];
    });

  }

  guardar(nombre:string, descripcion:string, url:string): boolean {
    const d = new DestinoViaje(nombre, descripcion, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): {[s: string]: boolean} {
    let l = control.value.toString().trim();
    if (!new TitleCasePipe().transform(l).match(l)) {
      return {invalidNombre: true};
    }
    return null;
  }

  nombreValidatorParam(minLong:number) : ValidatorFn {
    return (control:FormControl): {[s: string]: boolean} => {
      let l = control.value.toString().trim().length;
      if (l>0 && l<minLong) {
        return {minLongNombre: true};
      }
      return null;
    }
  }

}
